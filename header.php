<header class="page-header">
  <div class="row expanded align-middle">
    <div class="column medium-9">
      <?php the_custom_header_markup(); ?>
      <h1 class="site-title">
        <?php the_custom_logo(); ?>
        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a>
      </h1>
    </div>
    <div class="column medium-3 end hide-for-small-only">
      <img class="mmoser-logo" src="<?php echo get_template_directory_uri().'/assets/images/mmoser-logo.svg'; ?>">
    </div>
  </div>

  <p class="site-description hide">
    <?php bloginfo('description'); ?>
  </p>
  <?php if (has_nav_menu( 'top' )) : ?>
  <?php endif; ?>
</header>
