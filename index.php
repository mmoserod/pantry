<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <?php wp_head(); ?>
  </head>
  <body id="pantry">
    <?php get_header(); ?>
    <div id="primary" class="content-area">
      <main id="main" class="site-main" role="main">
        <div class="row medium-up-3 small-up-2" id="overview">
          <?php
          $galleries = new WP_Query([
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'orderby'        => 'rand'
          ]);
          foreach ($galleries->posts as $post) :?>
          <a class="column" href="#item-<?php echo $post->ID; ?>" data-smooth-scroll>
            <img src="<?php echo reset(get_post_gallery_images($post->ID)); ?>">
            <span><?php echo $post->post_title; ?></span>
          </a>
          <?php endforeach; ?>
        </div>
        <div class="row">
          <?php
            if (have_posts()) {
              while (have_posts()) {
                the_post();
                ?>
                <div class="column" style="margin-top:1rem;">
                  <div class="card" id="item-<?php echo get_the_ID(); ?>">
                    <div class="card-divider">
                      <h5><?php the_title(); ?></h5>
                    </div>
                    <div class="row expanded medium-up-2 hide-for-small-only">
                      <?php foreach (get_post_gallery_images() as $i => $pic) : ?>
                        <div class="column" style="background:url('<?php echo $pic; ?>') no-repeat center;<?php echo $i == 0 ? 'background-size:cover;'
                        : 'background-size:contain;' ?>">
                          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholder.png" />
                        </div>
                      <?php endforeach; ?>
                    </div>

                    <div class="show-for-small-only" id="slick-<?php echo get_the_ID(); ?>">
                      <?php foreach (get_post_gallery_images() as $i => $pic) : ?>
                        <div style="background:url('<?php echo $pic; ?>') no-repeat center;<?php echo $i == 0 ? 'background-size:cover;'
                        : 'background-size:contain;' ?>">
                          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholder.png" style="height:17rem;max-height:none;" />
                        </div>
                      <?php endforeach; ?>
                    </div>
                    <script>$(function() {$("#slick-<?php echo get_the_ID(); ?>").slick({slidesToShow:1,arrows:true});});</script>

                    <div class="card-section">
                      <div class="row medium-up-3 hide-for-small-only">
                        <div class="column"><a href="http://productivity2.mmoser.com/od/pantry-files/<?php echo get_post_meta(get_the_id(), 'pdf', true); ?>" class="button expanded secondary">DOWNLOAD .PDF VERSION</a></div>
                        <div class="column"><a href="http://productivity2.mmoser.com/od/pantry-files/<?php echo get_post_meta(get_the_id(), 'layout', true); ?>" class="button expanded secondary">DOWNLOAD .LAYOUT VERSION</a></div>
                        <div class="column"><a href="http://productivity2.mmoser.com/od/pantry-files/<?php echo get_post_meta(get_the_id(), 'skp', true); ?>" class="button expanded secondary">DOWNLOAD .SKP VERSION</a></div>
                      </div>
                      <div class="row show-for-small-only">
                        <div class="column small-4">
                          <a href="http://productivity2.mmoser.com/od/pantry-files/<?php echo get_post_meta(get_the_id(), 'pdf', true); ?>" class="button expanded secondary">
                            <i class="fa fa-download"></i> .PDF
                          </a>
                        </div>
                        <div class="column small-4">
                          <a href="http://productivity2.mmoser.com/od/pantry-files/<?php echo get_post_meta(get_the_id(), 'layout', true); ?>" class="button expanded secondary">
                            <i class="fa fa-download"></i> .LAYOUT
                          </a>
                        </div>
                        <div class="column small-4 end">
                          <a href="http://productivity2.mmoser.com/od/pantry-files/<?php echo get_post_meta(get_the_id(), 'skp', true); ?>" class="button expanded secondary">
                            <i class="fa fa-download"></i> .SKP
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
          ?>
        </div>
      </main>
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
    <a id="back" class="button large" href="#pantry">
      <i class="fa fa-chevron-up"></i>
    </a>
    <?php wp_footer(); ?>
    <style>
    .slick-prev {
      left: 0px;
      z-index: 2;
      background-color: rgba(0,0,0,0.5);
      padding: 0.5rem;
      width: auto;
      height: auto;
    }
    .slick-next {
      right: 0px;
      z-index: 2;
      background-color: rgba(0,0,0,0.5);
      padding: 0.5rem;
      width: auto;
      height: auto;
    }
    #back {
      position: fixed;
      bottom: 0.5rem;
      right: 0.5rem;
    }

    #overview .column {
      margin-top: 1rem;
    }
    #overview .column > span {
      background: #fff;
      font-size: 0.9rem;
      padding: 0.5rem;
      color: #000;
      display: block;
    }
    #overview .column:hover img {
      filter: brightness(80%) contrast(110%) saturate(80%);
    }
    </style>
  </body>
</html>
