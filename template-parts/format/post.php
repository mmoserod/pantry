<div id="post-<?php the_ID(); ?>" class="column">
  <div class="card">
    <div class="card-divider"><?php the_title(); ?></div>
    <?php the_post_thumbnail(); ?>
    <div class="card-section"><?php the_content(); ?></div>
  </div>
</div>
