<div id="post-<?php the_ID(); ?>" class="column gallery-card">
  <div class="row expanded">
    <div class="column medium-8">
      <div id="slick-<?php the_ID(); ?>" class="slick">
        <?php foreach (get_post_gallery_images() as $pic) : ?>
          <img src="<?php echo $pic; ?>">
        <?php endforeach; ?>
      </div>
    </div>
    <div class="column medium-4">
      <h6><?php the_title(); ?></h6>
      <div>
        <?php echo preg_replace("/\[.*\]/", "", get_the_content()); ?>
        <?php $pdf = get_post_meta(get_the_ID(), 'upload_sketchup', true); ?>
        <?php if (!empty($pdf)) : ?>
          <a href="<?php echo $pdf['url']?>" class="button expanded">DOWNLOAD PDF VERSION</a>
        <?php endif; ?>
        <a href="<?php echo $pdf['url']?>" class="button expanded">DOWNLOAD LAYOUT VERSION</a>
        <a href="<?php echo $pdf['url']?>" class="button expanded hollow">DOWNLOAD SKETCHUP FILE VERSION</a>
      </div>
    </div>
  </div>
</div>
