<?php
function queueScripts() {
  $v = "1.0.1";
  if (!is_admin()) {
    wp_deregister_script('jquery');

    wp_register_script('jquery', get_template_directory_uri().'/assets/js/vendor/jquery.js', false, $v);

    wp_enqueue_script('jquery');
    wp_enqueue_script('what-input', get_template_directory_uri().'/assets/js/vendor/what-input.js', false, $v);
    wp_enqueue_script('foundation', get_template_directory_uri().'/assets/js/vendor/foundation.min.js', false, $v);
    wp_enqueue_script('fancybox', get_template_directory_uri().'/assets/js/vendor/jquery.fancybox.min.js', false, $v);
    wp_enqueue_script('slick', get_template_directory_uri().'/assets/js/vendor/slick.min.js', false, $v);
    wp_enqueue_script('app', get_template_directory_uri().'/assets/js/app.js', true, $v);
  }
}

function queueStyles() {
  $v = "1.0.1";
  if (!is_admin()) {
    wp_enqueue_style('foundation', get_template_directory_uri().'/assets/css/vendor/foundation.min.css', false, $v, 'all');
    wp_enqueue_style('fancybox', get_template_directory_uri().'/assets/css/vendor/jquery.fancybox.min.css', false, $v, 'all');
    wp_enqueue_style('slick', get_template_directory_uri().'/assets/css/vendor/slick.css', false, $v, 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri().'/assets/css/vendor/slick-theme.css', false, $v, 'all');
    wp_enqueue_style('_fontawesome', get_template_directory_uri().'/assets/css/vendor/font-awesome.css', false, $v, 'all');
    wp_enqueue_style('app', get_template_directory_uri().'/assets/css/app.css', false, $v, 'all');
  }
}

add_theme_support('custom-logo');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', array('standard', 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
add_action('init', 'queueStyles');
add_action('init', 'queueScripts');
